﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[ExecuteInEditMode]
public class SetMaterial : MonoBehaviour
{
    
    public List<Material> garmentMaterial;

    public List<Texture> Main;
    public List<Texture> Specular;
    public List<Texture> Normal;

    Material outside;
    Material inside;

    GameObject garmentPart;  

    void Awake()
    {
        
        for (int i=0; i < gameObject.transform.childCount; i++)
        {
            garmentPart = gameObject.transform.GetChild(i).gameObject;

            if (garmentPart.name.Contains("Outside"))
            {
                outside = garmentMaterial[0];
                outside.EnableKeyword("_NORMALMAP");
                outside.EnableKeyword("_SpecGlossMap");
                outside.SetTexture("_MainTex", Main[0]);
                outside.SetTexture("_SpecGlossMap", Specular[0]);
                outside.SetFloat("_SpecGlossMap", 0);
                outside.SetTexture("_BumpMap", Normal[0]);
                outside.SetFloat("_BumpMap", 1);
                garmentPart.GetComponent<Renderer>().material = outside;
                //garmentMaterial[0] = outside;                

            }

            if (garmentPart.name.Contains("Inside"))
            {
                inside = garmentMaterial[1];
                inside.EnableKeyword("_NORMALMAP");
                inside.EnableKeyword("_SpecGlossMap");
                inside.SetTexture("_MainTex", Main[1]);
                inside.SetTexture("_SpecGlossMap", Specular[1]);
                inside.SetFloat("_SpecGlossMap", 0);
                inside.SetTexture("_BumpMap", Normal[1]);
                inside.SetFloat("_BumpMap", 1);
                garmentPart.GetComponent<Renderer>().material = inside;
                //garmentMaterial[1] = inside;               
               
            }

            
        }
        
        
    }
    
}
