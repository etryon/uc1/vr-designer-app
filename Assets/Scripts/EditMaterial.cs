﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditMaterial : MonoBehaviour
{

    public Material newMaterial;
    public Texture Main;
    public Texture Specular;
    public Texture Normal;

    private void Awake()
    {
        
        newMaterial.EnableKeyword("_NORMALMAP");

        newMaterial.EnableKeyword("_SpecGlossMap");


        newMaterial.SetTexture("_MainTex", Main);
        newMaterial.SetTexture("_SpecGlossMap", Specular);
        newMaterial.SetFloat("_SpecGlossMap", 0);
        newMaterial.SetTexture("_BumpMap", Normal);
        newMaterial.SetFloat("_BumpMap", 1);

        Debug.Log("Succesfully Edited Material");
    }

    private void FixedUpdate()
    {
        gameObject.GetComponent<Renderer>().material = newMaterial;
    }
}
