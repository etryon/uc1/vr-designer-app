﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectNew : MonoBehaviour
{
    //public List<GameObject> changeActive;

    public GameObject SelectedImage;
    public GameObject changeActive;

    public void ChangeSelected()
    {
        
        foreach(Transform child in SelectedImage.transform)
        {
            child.gameObject.SetActive(false);
        }
        changeActive.SetActive(true);

        //transform.parent.gameObject.SetActive(false);
    }
}
