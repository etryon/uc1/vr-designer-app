﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activate : MonoBehaviour
{
    public GameObject setactive;
    
    

    public void activate()
    {
        if (setactive.activeSelf)
        {
            setactive.SetActive(false);
        }
        else
        {
            setactive.SetActive(true);
        }
        
    }

}
