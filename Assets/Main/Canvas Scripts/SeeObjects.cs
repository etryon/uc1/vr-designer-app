﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeObjects : MonoBehaviour
{
    public GameObject parent;

    public GameObject spawnNew;

    public List<GameObject> deactivate;

    public void ShowActive()
    {
        if (parent.transform.childCount > 0)
        {
            foreach (Transform child in parent.transform)
            {
                child.gameObject.SetActive(false);
            }
        }


        spawnNew.SetActive(true);

        for (int i = 0; i < deactivate.Count; i++)
        {
            //deactivate[0].SetActive(false);
            //deactivate[1].SetActive(true);
            //foreach (Transform child in deactivate[i].transform)
            //{
            //    child.gameObject.SetActive(false);
            //}
        }

    }
}
