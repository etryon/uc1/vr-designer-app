﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActDeact : MonoBehaviour
{
    public GameObject setactive;
    public List<GameObject> deactivate;


    public void ActAndDeact()
    {

        for (int i = 0; i < deactivate.Count; i++)
        {
            foreach (Transform child in deactivate[i].transform)
            {
                child.gameObject.SetActive(false);
            }
        }            
        if (setactive.activeSelf)
        {
            setactive.SetActive(false);
        }
        else
        {
            setactive.SetActive(true);
        }
        
       
       
    }

}
