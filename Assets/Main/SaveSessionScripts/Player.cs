﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [System.Serializable]
    public class SaveableObjectsInScene
    {
        public SaveableObject[] SaveableObjects;
    }

    //Load Favorites based on Dropdown
    string DirectoryPath;
    public GameObject FavsTab;

    public GameObject LoadDrop;

    ObjectID[] ObjectsInScene;

    string currentFavorite;
    int k =0;
    [System.Serializable]
    public class SaveableObject
    {
        public int ID;
        public Vector3 WorldPosition;
        public Quaternion WorldRotation;        
    }
    
    public void Save()
    {
        k = 0;
        ObjectsInScene = FavsTab.transform.GetComponentsInChildren<ObjectID>();

        SaveableObjectsInScene ObjectData = new SaveableObjectsInScene
        {
            SaveableObjects = new SaveableObject[ObjectsInScene.Length]
        };

        for (int i = 0; i < ObjectData.SaveableObjects.Length; i++)
        {
            ObjectData.SaveableObjects[i] = new SaveableObject
            {
                ID = ObjectsInScene[i].ID,
                WorldPosition = ObjectsInScene[i].transform.position,
                WorldRotation = ObjectsInScene[i].transform.rotation
            };
        }
        Debug.Log("Saved");
        SaveSystem.Save(ObjectData);
        k = 1;
    }

    public void Load()
    {
        SaveSystem.Load(out SaveableObjectsInScene LoadedObjectData, currentFavorite);
    
        if (FavsTab.transform.childCount == 0)
        {
            k = 0;
        }

        if (k == 0)
        {
            for (int i = 0; i < LoadedObjectData.SaveableObjects.Length; i++)
            {

                var test = Instantiate(SaveableObjectLibrary.SaveableObjects[LoadedObjectData.SaveableObjects[i].ID], LoadedObjectData.SaveableObjects[i].WorldPosition, LoadedObjectData.SaveableObjects[i].WorldRotation);

                test.transform.SetParent(FavsTab.transform);

                var rectTransform = test.GetComponent<RectTransform>();
                rectTransform.sizeDelta = new Vector2(160f, 30f);
                rectTransform.localScale = new Vector3(1, 1, 1);
                test.transform.localPosition=new Vector3 (0, 0, 0);
                test.transform.localRotation = Quaternion.Euler(0, 0, 0);

                test.GetComponent<Button>().enabled = true;
                test.GetComponent<Image>().enabled = true;
                Debug.Log("Favorites Loaded");
                k = 1;
            }
        }
        else if (k == 1)
        {
            Debug.Log("Favorites already loaded!");
        }      
    }

    
    public void Awake()
    {
        DirectoryPath = $"{Application.dataPath}/Saves/";

        if (!Directory.Exists(DirectoryPath))
        {
            Directory.CreateDirectory(DirectoryPath);
        }
        
        var dropdown = LoadDrop.transform.GetComponent<Dropdown>();
        dropdown.options.Clear();

        List<string> savedFavorites = new List<string>();

        DirectoryInfo d = new DirectoryInfo(DirectoryPath);

        foreach (var file in d.GetFiles("*.json"))
        {            
            //Debug.Log(file.Name);

            var noType = file.Name.Split('.')[0];
            Debug.Log(noType);
            savedFavorites.Add(noType.ToString());
        }

        foreach (var item in savedFavorites)
        {
            dropdown.options.Add(new Dropdown.OptionData() { text = item });
        }

        currentFavorite = dropdown.options[0].text;
        dropdown.onValueChanged.AddListener(delegate {
            DropdownValueChanged(dropdown);
        });
    }

    void DropdownValueChanged(Dropdown dd)
    {
        currentFavorite = dd.options[dd.value].text;
        k = 0;
        Debug.Log(currentFavorite);
        
        foreach(Transform child in FavsTab.transform)
        {
            //child.gameObject.SetActive(false);
            Destroy(child.gameObject);
        }
    }

}

